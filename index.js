window.addEventListener("load", () => {
	const out = document.querySelector("#sq");
	const rgbaout = document.querySelector("#rgba");
	const hexout = document.querySelector("#hexstr");
	const red = document.querySelector("#red");
	const green = document.querySelector("#grn");
	const blue = document.querySelector("#blu");
	const alpha = document.querySelector("#aph");
	const rgba = [0,0,0,1];
	red.value = rgba[0];
	green.value = rgba[1];
	blue.value = rgba[2];
	alpha.value = rgba[3]*100;
	function update() {
		let stylestr = "width: 200px; height: 200px; background-color: rgba(" + rgba.join(",") + ");";
		function fl(hs) {
			return (hs.length == 1 ? "0"+hs : hs);
		}
		let ah = fl(Math.floor(rgba[3] * 255).toString(16));
		let rh = fl(rgba[0].toString(16));
		let gh = fl(rgba[1].toString(16));
		let bh = fl(rgba[2].toString(16));
		out.style = stylestr;
		hexstr.value = "#" + ah + rh + gh + bh;
		rgbaout.value = rgba.join(",");
	}
	function updateval(x) {
		let val = this.value;
		rgba[x] = parseInt(val);
		console.log(this);
		update();
	}
	red.oninput = function () {
		updateval.apply(this, [0]);
	}
	green.oninput = function () {
		updateval.apply(this, [1]);
	}
	blue.oninput = function () {
		updateval.apply(this, [2]);
	}
	alpha.oninput = function () {
		let val = this.value;
		rgba[3] = parseFloat(((1/100) * val).toString().substr(0,4));
		update();
	}
	update();
})